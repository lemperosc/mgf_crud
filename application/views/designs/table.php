<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<!-- JS FILE -->
	<script src="<?= base_url('assets/js/design.js') ?>"></script>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

</head>
<body>
    <h1 class="text-center">Designs</h1>
    <table class="table">
        <thead>
          <tr class="row">
            <td><strong>Id</strong></td>
            <td><strong>Design Id</strong></td>
            <td><strong>Category Id</strong></td> 
            <td><strong>Description</strong></td>
            <td><strong>Date In</strong></td>
            <td><strong>Date Out</strong></td>
            <td><strong>Designer</strong></td>
            <td><strong>Checker</strong></td>
            <td><strong>Status</strong></td>
            <td><strong>Design Type</strong></td>
            <td></td>
            <td></td>
          </tr>
        </thead>
        </tbody>
          <?php 
          foreach($designs as $di){?>
            <tr class="row" data-id= <?= $di->id?>>
              <td><?=$di->id;?></td>
              <td class="customer_id"><?=$di->customer_id;?></td> 
              <td class="location_id"><?=$di->location_id;?></td>
              <td class="contractor_id"><?=$di->contractor_id;?></td>
              <td class="title"><?=$di->title;?></td>
              <td class="special_project"><?=$di->special_project;?></td>
              <td class="permanent_works"><?=$di->permanent_works;?></td>
              <td class="depth"><?=$di->depth;?></td>
              <td class="length"><?=$di->length;?></td>
              <td class="width"><?=$di->width;?></td>
              <td class="design_type_id"><?=$di->design_type_id;?></td>
              <td><input type="submit" value="Edit" class="edit btn btn-warning"></td>
              <td><input type="submit" value="Delete" class="delete btn btn-danger"></td>
            </tr>    
          <?php }?> 
      	</tbody>
      </table>
	<button id="nD" class="btn btn-success">New Design</button>  
	<button id="back" class="btn btn-danger">Back</button>  
	<div class="update col-md-6"></div>
    </div>
    
    
    
</body>
</html>

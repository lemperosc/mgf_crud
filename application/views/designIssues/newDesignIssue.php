<form>
    <h2>Create new Design Issue</h2>
    
    <label for="design">Design</label>
    <input id="design"><br>


        <label for="category">Category</label>
        <select class="form-control" id="category">
            <option>0</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
        </select>
    <br>


    <label for="description">Description</label>
    <input id="description"><br>


    <label for="designer">Designer</label>
    <input id="designer"><br>


    <label for="checker">Checker</label>
    <input id="checker"><br>


    <label for="status">Status</label>
        <select class="form-control" id="status">
            <option>0</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
        </select>
    <br>


    <label for="type">Type</label>
    <input id="type"><br>
    
    <button id="sB" class="btn btn-primary">Submit</button>
</form>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Designs extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->database(); // load database
        $this->load->model('DesignIssue_Model'); // load model
        $this->load->model('Design_Model'); // load model
        $this->load->helper('url');
    }
	
	public function index()
	{
		$this->getDesignIssuesTable();
	}

	public function getDesignIssuesTable(){
		$data['designIssues'] = $this->DesignIssue_Model->getDesignIssues();
		$this->load->view('designIssues/designIssuesTable', $data);
	}

	public function newDesignIssue(){
		$issue = $this->input->post();
		if($issue == null){
			$view = $this->load->view('designIssues/newDesignIssue');
			echo json_encode($view);
		}else{
			$this->DesignIssue_Model->setDesignIssue($issue);
		}
	}

	public function editDesignIssue(){
		$issue = $this->input->post('issue');
		if(is_numeric($issue)){
			$data = $this->DesignIssue_Model->getDesignIssues();
			$view = $this->load->view('designIssues/editDesignIssue', $data[0]);
			echo json_encode($view);
		}else{
			$this->DesignIssue_Model->updateDesignIssue($issue);
		}
	}

	public function deleteDesignIssue(){
		$filter = $this->input->post('id');
		$this->DesignIssue_Model->deleteDesignIssue($filter);
	}

	public function getDesigns(){
		$data['designs'] = $this->Design_Model->getDesigns();
		$this->load->view('designs/table', $data);
	}


	public function newDesign(){
		$data = $this->input->post();
		if($data == null){
			$view = $this->load->view('designs/newDesign');
			echo json_encode($view);
		}else{
			$this->Design_Model->setDesign($data);
		}
	}

	public function editDesign(){
		$filter = $this->input->post('issue');
		if(is_numeric($filter)){
			$data = $this->DesignIssue_Model->getDesigns();
			$view = $this->load->view('designs/editDesign', $data[0]);
			echo json_encode($view);
		}else{
			$this->Design_Model->updateDesign($filter);
		}
	}

	public function deleteDesign(){
		$filter = $this->input->post('id');
		$this->DesignIssue_Model->deleteDesignIssue($filter);
	}

}

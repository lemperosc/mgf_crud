"use strict;"

$(() => {
	let urlParts 	= window.location.href.split("/index.php/");
    let base_url 	= urlParts[0] + "/";
    $(".delete").on('click', function(){
		let id = $(this).closest("tr").attr("data-id")
		$.ajax({
			url: base_url + 'index.php/Designs/deleteDesignIssue',
			type: 'POST',
			data: {id},
			success: function(data){
				location.reload()
			}
		})
	});

	$(".edit").on('click', function(){
		let issue = $(this).closest("tr").attr("data-id")
		$.ajax({
			url: base_url + 'index.php/Designs/editDesignIssue',
			type: 'POST',
			data: {issue},
			success: function(data){
				$('.update').html(data)
				updateIssue()
			}
		})
	});

	$("#nD").on('click', function(){
		$.ajax({
			url: base_url + 'index.php/Designs/newDesignIssue',
			type: 'GET',
			success: function(data){
				$('.update').html(data)
				newIssue()
				deleteIssue()
			}
		})
	});

	$("#gD").on('click', function(){
		$.ajax({
			url: base_url + 'index.php/Designs/getDesigns',
			type: 'GET',
			success: function(data){
				location.replace('Designs/getDesigns')
			}
		})
	});

	
});

function loadTable(){
	let urlParts 	= window.location.href.split("/index.php/");
	let base_url 	= urlParts[0] + "/";
	$.ajax({
		url: base_url + 'index.php/Designs/getDesignIssuesTable',
		type: 'GET',
		success: function(data){
			$("html").html(data)
		}
	})

}

function updateIssue(){
	let urlParts 	= window.location.href.split("/index.php/");
	let base_url 	= urlParts[0] + "/";
	$("#eB").on('click', function(){
		let issue = {
			id: $("h2").attr("data-id"),
			design_id: $("#design").val(),
			category_id: $("#category").val(),
			description: $("#description").val(),
			designer_id: $("#designer").val(), 
			checker_id: $("#checker").val(), 
			status_id: $("#status").val(), 
			drawing_req: $("#type").val()
		}
		$.ajax({
			url: base_url + 'index.php/Designs/editDesignIssue',
			type: 'POST',
			data: {issue},
			success: function(data){
				location.reload()
			}
		})
	});
}
function newIssue(){
	let urlParts 	= window.location.href.split("/index.php/");
	let base_url 	= urlParts[0] + "/";
	$("#sB").on('click', function(){
		let issue = {
			design_id: $("#design").val(),
			category_id: $("#category").val(),
			description: $("#description").val(),
			designer_id: $("#designer").val(), 
			checker_id: $("#checker").val(), 
			status_id: $("#status").val(), 
			drawing_req: $("#type").val()
		}
		console.log(issue)
		$.ajax({
			url: base_url + 'index.php/Designs/newDesignIssue',
			type: 'POST',
			data: issue,
			success: function(data){
				//location.reload()
			}
		})
	});
}

function deleteIssue(){
	let urlParts 	= window.location.href.split("/index.php/");
	let base_url 	= urlParts[0] + "/";
	
}


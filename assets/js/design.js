"use strict;"

$(() => {
    let urlParts 	= window.location.href.split("/index.php/");
    let base_url 	= urlParts[0] + "/";
    $("#back").on('click', function(){
		window.location.replace('http://localhost/mgf_crud/index.php/Designs')
    });

    $(".delete").on('click', function(){
		let id = $(this).closest("tr").attr("data-id")
		$.ajax({
			url: base_url + 'index.php/Designs/deleteDesignIssue',
			type: 'POST',
			data: {id},
			success: function(data){
				location.reload()
			}
		})
	});

	$(".edit").on('click', function(){
		let issue = $(this).closest("tr").attr("data-id")
		$.ajax({
			url: base_url + 'index.php/Designs/editDesignIssue',
			type: 'POST',
			data: {issue},
			success: function(data){
				$('.update').html(data)
				updateIssue()
			}
		})
	});

    
    $("#nD").on('click', function(){
		$.ajax({
			url: base_url + 'index.php/Designs/newDesign',
			type: 'GET',
			success: function(data){
				$('.update').html(data)
				newDesign()
			}
		})
	});
})

function updateDesign(){
	let urlParts 	= window.location.href.split("/index.php/");
	let base_url 	= urlParts[0] + "/";
	$("#eB").on('click', function(){
		let design = {
			customer_id: $("#customer").val(),
			location_id: $("#location").val(),
			contractor_id: $("#contractor").val(),
			title: $("#title").val(), 
			special_project: $("#special_project").val(), 
			permanent_works: $("#permanent_works").val(), 
			depth: $("#depth").val(),
			length: $("#length").val(),
			width: $("#width").val(),
			design_type_id: $("#design_type_id").val()
		}
		$.ajax({
			url: base_url + 'index.php/Designs/editDesign',
			type: 'POST',
			data: {design},
			success: function(data){
				location.reload()
			}
		})
	});
}
function newDesign(){
	let urlParts 	= window.location.href.split("/index.php/");
	let base_url 	= urlParts[0] + "/";
	$("#sB").on('click', function(){
        let design = {
			customer_id: $("#customer").val(),
			location_id: $("#location").val(),
			contractor_id: $("#contractor").val(),
			title: $("#title").val(), 
			special_project: $("#special_project").val(), 
			permanent_works: $("#permanent_works").val(), 
			depth: $("#depth").val(),
			length: $("#length").val(),
			width: $("#width").val(),
			design_type_id: $("#design_type_id").val()
		}
		$.ajax({
			url: base_url + 'index.php/Designs/newDesign',
			type: 'POST',
			data: design,
			success: function(data){
				location.reload()
			}
		})
	});
}